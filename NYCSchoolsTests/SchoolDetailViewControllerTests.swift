//
//  SchoolDetailViewControllerTests.swift
//  20210612-ManojKumarGajawada-NYCSchoolsTests
//
//  Created by Manoj Kumar Gajawada on 6/12/21.
//  Copyright © 2021 Manoj Kumar Gajawada. All rights reserved.
//

import Foundation
import XCTest
@testable import NYCSchools

class SchoolDetailViewControllerTests: XCTestCase {
    var sut: SchoolDetailViewController!
    
    override func setUp() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(identifier: "SchoolDetail") as? SchoolDetailViewController
        sut = viewController
    }
    
    func testDisplaySchoolDetails() {
        let schoolDetails = SchoolDetailModel(dbn: "02M374",
                                            schoolName: "GRAMERCY ARTS HIGH SCHOOL",
                                            satTestTakers: "60",
                                            satCriticalReadingAvgScore: "391",
                                            satMathAvgScore: "391",
                                            satWritingAvgScore: "394")
        sut.schoolDetails = schoolDetails
        sut.loadViewIfNeeded()
        
        XCTAssertTrue(sut.schoolDetails.dbn == "02M374")
        XCTAssertTrue(sut.schoolDetails.schoolName == "GRAMERCY ARTS HIGH SCHOOL")
        XCTAssertTrue(sut.schoolDetails.satTestTakers == "60")
        XCTAssertTrue(sut.schoolDetails.satCriticalReadingAvgScore == "391")
        XCTAssertTrue(sut.schoolDetails.satMathAvgScore == "391")
        XCTAssertTrue(sut.schoolDetails.satWritingAvgScore == "394")
    }

}
