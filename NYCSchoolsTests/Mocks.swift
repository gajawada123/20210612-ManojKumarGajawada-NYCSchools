//
//  Mocks.swift
//  20210612-ManojKumarGajawada-NYCSchoolsTests
//
//  Created by Manoj Kumar Gajawada on 6/12/21.
//  Copyright © 2021 Manoj Kumar Gajawada. All rights reserved.
//

import XCTest
@testable import NYCSchools

class MockSchoolViewModel: SchoolViewModel {
    var getSchoolsDataSuccess = true
    var getSchoolsDataSuccessExecuted = false
    var getSchoolsDataFailureExecuted = false
    var mocknumOfSchools = 5
    var mockDBN = "110"
    
    override func getSchoolsList(onSuccess success: @escaping () -> Void, onFailure failure: @escaping () -> Void) {
        if getSchoolsDataSuccess {
            getSchoolsDataSuccessExecuted = true
            success()
        } else {
            getSchoolsDataFailureExecuted = true
            failure()
        }
    }
    
    override func numOfSchools() -> Int {
        mocknumOfSchools
    }
    
    override func getSchoolModel(for index: Int) -> SchoolModel? {
        return SchoolModel(dbn: mockDBN, schoolName: "New Explorers High School")
    }
}

class MockSchoolDetailsViewModel: SchoolDetailsViewModel {
    var getSchoolDetailsDataSuccess = true
    var getSchoolDetailsDataSuccessExecuted = false
    var getSchoolDetailsDataFailureExecuted = false
    
    override func getSchoolsDetailsData(onSuccess success: @escaping () -> Void, onFailure failure: @escaping () -> Void) {
        if getSchoolDetailsDataSuccess {
            getSchoolDetailsDataSuccessExecuted = true
            success()
        } else {
            getSchoolDetailsDataFailureExecuted = true
            failure()
        }
    }

    override func schoolsDetail() -> [SchoolDetailModel]? {
        var schoolDetails = [SchoolDetailModel]()
        let schoolDetails1 = SchoolDetailModel(dbn: "100",
                                               schoolName: "Test School Name1",
                                               satTestTakers: "101",
                                               satCriticalReadingAvgScore: "102",
                                               satMathAvgScore: "200",
                                               satWritingAvgScore: "250")
        let schoolDetails2 = SchoolDetailModel(dbn: "101",
                                               schoolName: "Test School Name2",
                                               satTestTakers: "101",
                                               satCriticalReadingAvgScore: "102",
                                               satMathAvgScore: "200",
                                               satWritingAvgScore: "250")
        let schoolDetails3 = SchoolDetailModel(dbn: "102",
                                               schoolName: "Test School Name3",
                                               satTestTakers: "101",
                                               satCriticalReadingAvgScore: "102",
                                               satMathAvgScore: "200",
                                               satWritingAvgScore: "250")
        schoolDetails.append(schoolDetails1)
        schoolDetails.append(schoolDetails2)
        schoolDetails.append(schoolDetails3)
        
        return schoolDetails
    }
}

class MockNavigationController: UINavigationController {
    var pushViewControllerCalled = false
    var presentViewControllerCalled = false
    var controllerPresented: UIViewController?
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushViewControllerCalled = true
    }
    
    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        presentViewControllerCalled = true
        controllerPresented = viewControllerToPresent
    }
}
