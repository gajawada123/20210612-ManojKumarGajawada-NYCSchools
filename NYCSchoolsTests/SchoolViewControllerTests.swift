//
//  SchoolViewControllerTests.swift
//  NYCSchoolsTests
//
//  Created by Manoj Kumar Gajawada on 6/13/21.
//  Copyright © 2021 Manoj Kumar Gajawada. All rights reserved.
//

import Foundation
import XCTest
@testable import NYCSchools

class MockTableView: UITableView {
    var expectation: XCTestExpectation?
    var reloadDataExecuted = false
    var identifier: String = "test"

    override func reloadData() {
        reloadDataExecuted = true
    }
}

class SchoolViewControllerTests: XCTestCase {
    var sut: ViewController!
    var mockTableView: MockTableView!
    var mockSchoolViewModel: MockSchoolViewModel!
    var mockSchoolDetailsViewModel: MockSchoolDetailsViewModel!
    var cellSut: SchoolViewCell!
    var mockNavigationController: MockNavigationController!

    override func setUp() {
        mockTableView = MockTableView()
        mockSchoolViewModel = MockSchoolViewModel()
        mockSchoolDetailsViewModel = MockSchoolDetailsViewModel()
        mockNavigationController = MockNavigationController()
        sut = ViewController(schoolViewModel: mockSchoolViewModel,
                             schoolDetailViewModel: mockSchoolDetailsViewModel,
                             tableView: mockTableView,
                             navController: mockNavigationController)
    }

    override func tearDown() {
        sut = nil
    }
    
    func testViewDidLoad() {
        let expectation = self.expectation(description: "testNone")
        
        mockSchoolViewModel.getSchoolsDataSuccess = true
        sut.viewDidLoad()
        
        DispatchQueue.main.async {
            expectation.fulfill()
        }
        waitForExpectations(timeout: 2, handler: nil)
        
        XCTAssertTrue(mockSchoolViewModel.getSchoolsDataSuccessExecuted)
        XCTAssertTrue(self.mockTableView.reloadDataExecuted)
    }
    
    func testViewDidLoad_FetchingFailure() {
        let expectation = self.expectation(description: "testNone")
        
        mockSchoolViewModel.getSchoolsDataSuccess = false
        sut.viewDidLoad()
        
        DispatchQueue.main.async {
            expectation.fulfill()
        }
        waitForExpectations(timeout: 2, handler: nil)
        
        XCTAssertFalse(mockSchoolViewModel.getSchoolsDataSuccessExecuted)
        XCTAssertFalse(self.mockTableView.reloadDataExecuted)
    }
    
    func testNumberofRowsInSection() {
        mockSchoolViewModel.mocknumOfSchools = 10
        let rows = sut.tableView(UITableView(), numberOfRowsInSection: 0)
        XCTAssertEqual(rows, 10)
    }
    
    func testDidSelectRow_SchoolDetailAvailable() {
        let indexPath = IndexPath(row: 0, section: 0)
        mockSchoolViewModel.mockDBN = "101"
        sut.tableView(UITableView(), didSelectRowAt: indexPath)
        XCTAssertTrue(mockNavigationController.pushViewControllerCalled)
    }
    
    func testDidSelectRow_SchoolDetailNotAvailable() {
        let indexPath = IndexPath(row: 0, section: 0)
        mockSchoolViewModel.mockDBN = "123"
        sut.tableView(UITableView(), didSelectRowAt: indexPath)
        XCTAssertFalse(mockNavigationController.pushViewControllerCalled)
        XCTAssertTrue(mockNavigationController.presentViewControllerCalled)
        if let controllerPresented = mockNavigationController.controllerPresented {
            XCTAssertTrue(controllerPresented.isKind(of: UIAlertController.self))
        }
    }
}
