//
//  SchoolDetailsViewModel.swift
//  NYCSchools
//
//  Created by Manoj Kumar Gajawada on 6/13/21.
//  Copyright © 2021 Manoj Kumar Gajawada. All rights reserved.
//

import Foundation

class SchoolDetailsViewModel {
    fileprivate var schoolDetails: [SchoolDetailModel]?
    
    func getSchoolsDetailsData(onSuccess success:@escaping () -> Void, onFailure failure:@escaping () -> Void) {
        let urlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { data, response, error in
                if error == nil, let data = data {
                    do {
                        let decoder = JSONDecoder()
                        self.schoolDetails = try decoder.decode([SchoolDetailModel].self, from: data)
                        success()
                    } catch  {
                        self.schoolDetails = nil
                        failure()
                    }
                } else {
                    self.schoolDetails = nil
                    failure()
                }
            }.resume()
        }
    }
    
    func schoolsDetail() -> [SchoolDetailModel]? {
        return self.schoolDetails
    }
}
