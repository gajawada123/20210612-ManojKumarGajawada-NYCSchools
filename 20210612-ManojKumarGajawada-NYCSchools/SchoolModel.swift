//
//  SchoolModel.swift
//  20210612-ManojKumarGajawada-NYCSchools
//
//  Created by Manoj Kumar Gajawada on 6/12/21.
//  Copyright © 2021 Manoj Kumar Gajawada. All rights reserved.
//

import Foundation

struct SchoolModel: Codable {
    var dbn: String
    var schoolName: String
    
    public enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
    }
}

struct SchoolDetailModel: Codable {
    var dbn: String
    var schoolName: String
    var satTestTakers: String
    var satCriticalReadingAvgScore: String
    var satMathAvgScore: String
    var satWritingAvgScore: String
    
    public enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case satTestTakers = "num_of_sat_test_takers"
        case satCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case satMathAvgScore = "sat_math_avg_score"
        case satWritingAvgScore = "sat_writing_avg_score"
    }
}
