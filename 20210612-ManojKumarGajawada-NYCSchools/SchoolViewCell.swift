//
//  SchoolViewCell.swift
//  20210612-ManojKumarGajawada-NYCSchools
//
//  Created by Manoj Kumar Gajawada on 6/12/21.
//  Copyright © 2021 Manoj Kumar Gajawada. All rights reserved.
//

import Foundation
import UIKit

class SchoolViewCell: UITableViewCell {
    @IBOutlet weak var schoolName: UILabel?
    
    func configureCell(model: SchoolModel) {
        schoolName?.text = model.schoolName
    }
}
