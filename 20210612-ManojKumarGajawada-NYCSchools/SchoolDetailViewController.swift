//
//  SchoolDetailViewController.swift
//  20210612-ManojKumarGajawada-NYCSchools
//
//  Created by Manoj Kumar Gajawada on 6/12/21.
//  Copyright © 2021 Manoj Kumar Gajawada. All rights reserved.
//

import Foundation
import UIKit

class SchoolDetailViewController: UIViewController {
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var satTestTakers: UILabel!
    @IBOutlet weak var satCriticalReadingAvgScore: UILabel!
    @IBOutlet weak var satMathAvgScore: UILabel!
    @IBOutlet weak var satWritingAvgScore: UILabel!
    var schoolDetails: SchoolDetailModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "School Details"
        displaySchoolDetails()
        
    }
    
    func displaySchoolDetails() {
        schoolName.text = schoolDetails.dbn + " : " + schoolDetails.schoolName
        satTestTakers.text = schoolDetails.satTestTakers
        satCriticalReadingAvgScore.text = schoolDetails.satCriticalReadingAvgScore
        satMathAvgScore.text = schoolDetails.satMathAvgScore
        satWritingAvgScore.text = schoolDetails.satWritingAvgScore
    }
    
    
}
