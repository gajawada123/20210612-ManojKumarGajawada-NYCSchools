//
//  ViewController.swift
//  20210612-ManojKumarGajawada-NYCSchools
//
//  Created by Manoj Kumar Gajawada on 6/12/21.
//  Copyright © 2021 Manoj Kumar Gajawada. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet private weak var tableView: UITableView!
    private var schoolViewModel = SchoolViewModel()
    private var schoolDetailViewModel = SchoolDetailsViewModel()
    private lazy var navController: UINavigationController? = {
        self.navigationController
    }()
    
    // Used convenience init in order to inject the dependencies through initializers. This is used for unit tests.
    convenience init(schoolViewModel: SchoolViewModel,
                     schoolDetailViewModel: SchoolDetailsViewModel,
                     tableView: UITableView,
                     navController: UINavigationController) {
        self.init()
        self.schoolViewModel = schoolViewModel
        self.schoolDetailViewModel = schoolDetailViewModel
        self.tableView = tableView
        self.navController = navController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "List of NYC Schools"
        getSchoolsList()
    }
    
    /* Fetching the list of schools by calling the api with success and failure blocks.
    In success block we are reloading tableview to display the results.
    In failure block, we are displaying the alert that details are not available*/
    private func getSchoolsList() {
        schoolViewModel.getSchoolsList(onSuccess: {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }) {
            DispatchQueue.main.async {
                self.showAlert()
            }
        }
    }

    // number of rows will be returned based on the schools count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        schoolViewModel.numOfSchools()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolViewCell", for: indexPath)
        if let schoolCell = cell as? SchoolViewCell, let school = schoolViewModel.getSchoolModel(for: indexPath.row) {
            schoolCell.configureCell(model: school)
            cell.selectionStyle = .none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard  let schoolsDetails = schoolDetailViewModel.schoolsDetail() else {
            schoolDetailViewModel.getSchoolsDetailsData(onSuccess: {
                DispatchQueue.main.async {
                    let schoolsDetails = self.schoolDetailViewModel.schoolsDetail()
                    self.goToSchoolDeatil(schoolsDetails: schoolsDetails, indexPath: indexPath)
                }
            }) {
                DispatchQueue.main.async {
                    self.showAlert()
                }
            }
            return
        }
        goToSchoolDeatil(schoolsDetails: schoolsDetails, indexPath: indexPath)
    }
    
    // comparing the dbn values as unique identifier for each school and retrieving the school details based on it
    private func goToSchoolDeatil(schoolsDetails: [SchoolDetailModel]?, indexPath: IndexPath) {
        if let data = schoolsDetails, let schoolDbn = schoolViewModel.getSchoolModel(for: indexPath.row)?.dbn {
            let school = data.first{ $0.dbn == schoolDbn }
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let vc = storyboard.instantiateViewController(identifier: "SchoolDetail") as? SchoolDetailViewController, school != nil {
                vc.schoolDetails = school
                self.navController?.pushViewController(vc, animated: true)
            } else {
                showAlert()
            }
        } else {
            showAlert()
        }
    }
    
    //display an alert when no data is available
    private func showAlert() {
        let alert = UIAlertController(title: "Error", message: "School Score Details Not Found", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        navController?.present(alert, animated: true, completion: nil)
    }
}

