//
//  SchoolViewModel.swift
//  20210612-ManojKumarGajawada-NYCSchools
//
//  Created by Manoj Kumar Gajawada on 6/12/21.
//  Copyright © 2021 Manoj Kumar Gajawada. All rights reserved.
//

import Foundation

class SchoolViewModel {
    fileprivate var schoolsList: [SchoolModel]?
    
    func getSchoolsList(onSuccess success:@escaping () -> Void, onFailure failure:@escaping () -> Void) {
        let urlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { data, response, error in
                if error == nil, let data = data {
                    do {
                        let decoder = JSONDecoder()
                        let schoolData = try decoder.decode([SchoolModel].self, from: data)
                        self.schoolsList = schoolData
                        success()
                    } catch {
                        self.schoolsList = nil
                        failure()
                    }
                } else {
                    self.schoolsList = nil
                    failure()
                }
            }.resume()
        }
    }
    
    func numOfSchools() -> Int {
        return self.schoolsList?.count ?? 0
    }
    
    func getSchoolModel(for index: Int) -> SchoolModel? {
        guard index < numOfSchools() else {
            fatalError("Use numberOfprovider before using this")
        }
        return self.schoolsList?[index]
    }
}
